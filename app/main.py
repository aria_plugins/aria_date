import datetime
import dateutil.parser
import locale

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        if request.json['lang'] == 'fr':
            locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')
        else:
            locale.setlocale(locale.LC_ALL, 'en_US.utf8')

        outcome = request.json['outcome']
        if 'entities' in outcome:
            if 'datetime' in outcome['entities']:
                if len(outcome['entities']['datetime']) > 0:
                    if 'value' in outcome['entities']['datetime'][0]:
                        date = dateutil.parser.parse(outcome['entities']['datetime'][0]['value'])
                        if request.json['lang'] == 'fr':
                            return date.strftime('%A %d-%m-%Y')
                        elif request.json['lang'] == 'en':
                            return date.strftime('%A %Y-%m-%d')
                        return date.strftime('%A %Y-%m-%d')

        if request.json['lang'] == 'fr':
            return datetime.datetime.now().strftime('%A %d-%m-%Y')
        elif request.json['lang'] == 'en':
            return datetime.datetime.now().strftime('%A %Y-%m-%d')
        return datetime.datetime.now().strftime('%A %Y-%m-%d')

if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
